#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', ]

with open('requirements_dev.txt') as requirements_dev_file:
    setup_requirements = requirements_dev_file.read()


test_requirements = [ ]

setup(
    author="Henrik Åhl",
    author_email='hpa22@cam.ac.uk',
    python_requires='!=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="imgmisc",
#    entry_points={
#        'console_scripts': [
#            'imgmisc=imgmisc.cli:main',
#        ],
#    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='imgmisc',
    name='imgmisc',
    packages=find_packages(include=['imgmisc', 'imgmisc.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/supersubscript/imgmisc',
    version='0.2.1',
    zip_safe=False,
)
