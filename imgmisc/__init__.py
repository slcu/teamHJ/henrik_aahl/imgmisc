# -*- coding: utf-8 -*-

"""Top-level package for imgmisc."""

import warnings
from .imgmisc import *

__author__ = """Henrik Åhl"""
__email__ = 'hpa22@cam.ac.uk'
__version__ = '0.2.1'
