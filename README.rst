=======
imgmisc
=======


.. image:: https://img.shields.io/pypi/v/imgmisc.svg
        :target: https://pypi.python.org/pypi/imgmisc

.. image:: https://img.shields.io/travis/supersubscript/imgmisc.svg
        :target: https://travis-ci.org/supersubscript/imgmisc

.. image:: https://readthedocs.org/projects/imgmisc/badge/?version=latest
        :target: https://imgmisc.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




imgmisc


* Free software: GNU General Public License v3
* Documentation: https://imgmisc.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
